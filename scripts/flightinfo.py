import sys

sys.path.append('/home/andreyb/flightaware/flightinfo/src')

username = ''
api_key = ''


if __name__ == '__main__':
    from flightaware import FlightAware

    if not username or not api_key:
        print('Please set configuration variables')
        sys.exit(1)

    if len(sys.argv) < 2:
        print("python3 flightinfo.py <ident>")
        sys.exit(1)

    fa = FlightAware(username, api_key)
    result = fa.FlightInfoCustom(sys.argv[1])

    if result:
        print(result.to_dict())
    else:
        print('No result')