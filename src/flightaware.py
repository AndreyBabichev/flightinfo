# -*- coding: utf-8 -*-
import re
import logging
from requests import request
from requests import exceptions
from requests.models import codes


class FlightInfo(object):

    AIRLINE_RE = re.compile('^([A-Z]+)([0-9]+)')

    STATUS_CANCELED = -1
    STATUS_UNKNOWN = 0
    STATUS_NOT_DEPARTED = 1
    STATUS_DELAYED = 2

    def __init__(self, ident, data):
        self.ident = ident
        self.data = data

    @property
    def airline(self):
        airline = None
        match = self.AIRLINE_RE.match(self.ident.upper())
        if match:
            airline = match.groups()[0]

        return airline

    @property
    def status(self):
        actualdeparturetime = self.data['actualdeparturetime']
        filed_departuretime = self.data['filed_departuretime']

        if actualdeparturetime < 0:
            return self.STATUS_CANCELED

        if actualdeparturetime == 0:
            return self.STATUS_NOT_DEPARTED

        if actualdeparturetime > filed_departuretime:
            return self.STATUS_DELAYED

        return self.STATUS_UNKNOWN

    @property
    def departure_delay(self):
        actualdeparturetime = self.data['actualdeparturetime']
        filed_departuretime = self.data['filed_departuretime']

        if self.STATUS_DELAYED:
            return actualdeparturetime - filed_departuretime

        return 0

    def to_dict(self):
        return {
            'airline': self.airline,
            'status': self.status,
            'departure_delay': self.departure_delay / 60 if self.departure_delay > 0 else 0
        }


# noinspection PyPep8Naming
class FlightAware(object):

    LOGGER = logging.getLogger('FlightAware')
    BASE_URL = "https://flightxml.flightaware.com/json/FlightXML2/"

    ERR_MSG = {
        401: 'Invalid credentials'
    }

    class ApiError(Exception):

        def __init__(self, message, error=None):
            super(FlightAware.ApiError, self).__init__(message)

            self.error = error

    def __init__(self, username, api_key, url=None):
        self.url = url or self.BASE_URL
        self.username = username
        self.api_key = api_key

    def call_api(self, api_method, params=None):
        """
        :param api_method: Remote API method name
        :param params: Query string params
        :raises: requests.exceptions.RequestException or requests.exceptions.ConnectionError
        :rtype: object
        :return: Response raw content or serialized json object.
        """
        response = request('get', self.url + api_method, params=params, auth=(self.username, self.api_key))

        if not str(response.status_code).startswith('2'):
            raise exceptions.RequestException(self.ERR_MSG.get(response.status_code, 'Unexpected server error'),
                                              codes[response.status_code],
                                              request=request, response=response)

        if response.headers.get('Content-Type', '').startswith('application/json'):
            return response.json()

        return response.content

    # noinspection PyPep8Naming
    def FlightsInfoEx(self, ident, howMany=1, offset=0):
        """
        https://ru.flightaware.com/commercial/flightxml/explorer/#op_FlightInfoEx

        Response example:
        {
          "FlightInfoExResult": {
            "next_offset": 1,
            "flights": [
              {
                "faFlightID": "AFL2354-1548743151-airline-0382",
                "ident": "AFL2354",
                "aircrafttype": "A321",
                "filed_ete": "02:45:00",
                "filed_time": 1548743151,
                "filed_departuretime": 1548956700,
                "filed_airspeed_kts": 327,
                "filed_airspeed_mach": "",
                "filed_altitude": 0,
                "route": "",
                "actualdeparturetime": 0,
                "estimatedarrivaltime": 1548966600,
                "actualarrivaltime": 0,
                "diverted": "",
                "origin": "UUEE",
                "destination": "LOWW",
                "originName": "Шереметьево",
                "originCity": "Moscow",
                "destinationName": "Вена-Швехат",
                "destinationCity": "Vienna"
              }
            ]
          }
        }
        :param ident: requested tail number, or airline with flight number, or faFlightID, or "ident@departureTime"
        :type ident: str
        :param howMany: maximum number of past flights to obtain. Must be a positive integer value less than or equal
        to 15, unless SetMaximumResultSize has been called.
        :type howMany: int
        :param offset: 	must be an integer value of the offset row count you want the search to start at. Most requests
        should be 0.
        :type offset: int
        :rtype: list
        """
        result = self.call_api('FlightInfoEx', {'ident': ident, 'howMany': howMany, 'offset': offset})

        if isinstance(result, dict):
            error_msg = result.get('error', '')

            if error_msg:
                if error_msg == 'no data available':
                    return []
                raise self.ApiError(error_msg)

            return result['FlightInfoExResult']['flights']

        raise self.ApiError('Unexpected response')

    def FlightInfoEx(self, ident):
        result = self.FlightsInfoEx(ident)

        if len(result) > 0:
            return result[0]

        return None

    def FlightInfoCustom(self, ident):
        """
        :type ident: str
        """
        result = self.FlightInfoEx(ident)

        if result:
            return FlightInfo(ident, result)

        return None