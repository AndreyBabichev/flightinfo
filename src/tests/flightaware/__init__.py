# -*- coding: utf-8 -*-
import sys
import requests
import responses
import unittest

sys.path.append('/home/andreyb/flightaware/flightinfo/src')


from flightaware import FlightAware, FlightInfo


class BaseTestCase(unittest.TestCase):

    @property
    def FlightInfoExResult(self):
        return {
            "FlightInfoExResult": {
                "next_offset": 1,
                "flights": [
                    {
                        "faFlightID": "AFL2354-1548743151-airline-0382",
                        "ident": "AFL2354",
                        "aircrafttype": "A321",
                        "filed_ete": "02:45:00",
                        "filed_time": 1548743151,
                        "filed_departuretime": 1548956700,
                        "filed_airspeed_kts": 327,
                        "filed_airspeed_mach": "",
                        "filed_altitude": 0,
                        "route": "",
                        "actualdeparturetime": 0,
                        "estimatedarrivaltime": 1548966600,
                        "actualarrivaltime": 0,
                        "diverted": "",
                        "origin": "UUEE",
                        "destination": "LOWW",
                        "originName": "Шереметьево",
                        "originCity": "Moscow",
                        "destinationName": "Вена-Швехат",
                        "destinationCity": "Vienna"
                    }
                ]
            }
        }

    def assertMyEqual(self, expected_obj, data_obj, exclude_fields=None, sorted_list=True):
        """
        :type expected_obj: object
        :type data_obj: object
        :type exclude_fields: list
        :type sorted_list: bool
        :param sorted_list: sorts lists during validation
        """
        if isinstance(expected_obj, dict):
            assert isinstance(data_obj, dict), "second argument must be 'dict'"

            self.assertMyDictEqual(expected_obj, data_obj, exclude_fields=exclude_fields, sorted_list=sorted_list)
        elif isinstance(expected_obj, (list, tuple)):
            assert isinstance(data_obj, (list, tuple)), "second argument must be 'list' or 'tuple'"

            self.assertMyListEqual(expected_obj, data_obj, exclude_fields=exclude_fields, sorted_list=sorted_list)
        else:
            expected_obj = str(expected_obj).decode('utf-8') if isinstance(expected_obj, bytes) else expected_obj
            data_obj = str(data_obj).decode('utf-8') if isinstance(data_obj, bytes) else data_obj
            self.assertEqual(expected_obj, data_obj)

    def assertMyDictEqual(self, expected_dict, data_dict, exclude_fields=None, sorted_list=True):
        """
        :type expected_dict: dict
        :type data_dict: dict
        :type exclude_fields: list
        :type sorted_list: bool
        :param sorted_list: sorts lists during validation
        """
        for field, expected_value in expected_dict.items():
            if exclude_fields and field in exclude_fields:
                continue
            try:
                value = data_dict[field]
            except KeyError:
                raise self.failureException(
                    (
                        "Matched dictionary does not contain the key '%s'\n"
                        "Expected: %s\n"
                        "Matched: %s\n"
                    ) % (field, expected_dict, data_dict)
                )

            try:
                self.assertMyEqual(expected_value, value, exclude_fields=exclude_fields, sorted_list=sorted_list)
            except self.failureException as err:
                raise self.failureException(str(err) + " field '%s'" % field)

        if len(expected_dict.keys()) != len(data_dict.keys()):
            missed_keys = []
            exclude_fields = exclude_fields or ()
            for k in data_dict.keys():
                if k not in expected_dict and exclude_fields and k not in exclude_fields:
                    missed_keys.append(k)
            if missed_keys:
                raise self.failureException("The expected value missed next keys: %s." % missed_keys)

    def assertMyListEqual(self, expected_list, data_list, exclude_fields=None, sorted_list=True):
        """
        :type expected_list: list or tuple
        :type data_list: list or tuple
        :type exclude_fields: list
        :type sorted_list: bool
        :param sorted_list: sorts lists during validation
        """
        if len(expected_list) != len(data_list):
            raise self.failureException("Expected %s elements, provided %s" % (len(expected_list), len(data_list)))

        if sorted_list:
            expected_list = sorted(expected_list)
            data_list = sorted(data_list)

        for n in range(len(expected_list)):
            expected_item = expected_list[n]
            data_item = data_list[n]

            try:
                self.assertMyEqual(expected_item, data_item, exclude_fields=exclude_fields, sorted_list=sorted_list)
            except self.failureException as err:
                raise self.failureException(
                    str(err) + " at %s item,\nExpected: %s\nOrigin: %s" %
                    (n, expected_item, data_item)
                )


class FlightAwareTestCase(BaseTestCase):

    test_url = 'https://flightxml.flightaware.com/json/FlightXML2/FlightInfoEx?ident=%s&howMany=1&offset=0'

    def get_url(self, ident):
        return self.test_url % ident

    @responses.activate
    def test_unauthorized(self):
        responses.add(responses.GET, self.get_url('ident'), status=401)

        fa = FlightAware('username', 'api_key')

        with self.assertRaises(requests.exceptions.RequestException) as context:
            fa.FlightInfoEx('ident')

        self.assertIn('Invalid credentials', str(context.exception))

    @responses.activate
    def test_FlightsInfoEx(self):
        json_data = self.FlightInfoExResult
        fa = FlightAware('username', 'api_key')

        responses.add(
            responses.GET,
            self.get_url('ident'),
            json=json_data,
            status=200
        )

        result = fa.FlightsInfoEx('ident')

        self.assertIsInstance(result, list)
        self.assertTrue(len(result) > 0)
        self.assertMyDictEqual(json_data['FlightInfoExResult']['flights'][0], result[0])

    @responses.activate
    def test_FlightInfoEx(self):
        json_data = self.FlightInfoExResult
        json_data2 = {
            'FlightInfoExResult': {
                'flights': []
            }
        }

        fa = FlightAware('username', 'api_key')
        url = self.get_url('ident')
        responses.add(
            responses.GET,
            url,
            json=json_data,
            status=200
        )
        # Empty list
        responses.add(
            responses.GET,
            url,
            json=json_data2,
            status=200
        )
        # error: not found
        responses.add(
            responses.GET,
            url,
            json={'error': 'no data available'},
            status=200
        )

        result = fa.FlightInfoEx('ident')

        self.assertIsInstance(result, dict)
        self.assertMyDictEqual(json_data['FlightInfoExResult']['flights'][0], result)

        self.assertIsNone(fa.FlightInfoEx('ident'))
        self.assertIsNone(fa.FlightInfoEx('ident'))

    @responses.activate
    def test_FlightInfoCustom(self):
        json_data = self.FlightInfoExResult

        fa = FlightAware('username', 'api_key')

        responses.add(
            responses.GET,
            self.get_url('SU2354'),
            json=json_data,
            status=200
        )

        result = fa.FlightInfoCustom('SU2354')

        self.assertIsInstance(result, FlightInfo)


class FlightInfoTestCase(BaseTestCase):

    def test_to_dict(self):
        data_item = self.FlightInfoExResult['FlightInfoExResult']['flights'][0]

        # Not departed
        data_item['actualdeparturetime'] = 0
        fi = FlightInfo('SU123', data_item)

        self.assertMyDictEqual(
            {'status': FlightInfo.STATUS_NOT_DEPARTED, 'departure_delay': 0, 'airline': 'SU'},
            fi.to_dict()
        )

        # Delayed
        data_item['actualdeparturetime'] = data_item['filed_departuretime'] + 300 * 60
        fi = FlightInfo('SU123', data_item)

        self.assertMyDictEqual(
            {'status': FlightInfo.STATUS_DELAYED, 'departure_delay': 300, 'airline': 'SU'},
            fi.to_dict()
        )

        #Canceled
        data_item['actualdeparturetime'] = -1
        fi = FlightInfo('SU123', data_item)

        self.assertMyDictEqual(
            {'status': FlightInfo.STATUS_CANCELED, 'departure_delay': 0, 'airline': 'SU'},
            fi.to_dict()
        )

if __name__ == '__main__':
    unittest.main()